const express = require('express');
const router = express.Router();
const {agregar_plato, modificar_plato,  eliminar_plato, obtener_plato}= require('../controllers/platos.controlles');
const plato_no_repetido = require('../middlewares/plato_no_repetido.middleware')
const plato_existe = require('../middlewares/plato_existe.middleware');
const validar_datos_plato = require('../middlewares/validar_datos_plato_middleware');



/**
 * @swagger
 * /platos:
 *  get:
 *      summary: Obtener todos los platos  del sistema
 *      tags: [platos]
 *      responses:
 *          200:
 *              description: Lista de prodcutos del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Agregar'
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 *          404:
 *              description: Peticion rechazada
 *          500:
 *              description: Error en el servidor
 */
router.get('/', obtener_plato);


/**
 * @swagger
 * /platos/{id_plato}:
 *  delete:
 *      summary: elimina un plato en el sistema
 *      tags: [platos]
 *      parameters:
 *          - in: path
 *            name: id_plato
 *            schema:
 *                 $ref: '#/components/schemas/Modificar'
 *            required: true
 *      responses:
 *          200:
 *              description: plato eliminado
 *          400:
 *              description: Peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.delete('/:id_plato', plato_existe,  eliminar_plato);

router.use(validar_datos_plato);

/**
 * @swagger
 * /platos:
 *  post:
 *      summary: Crea un plato en el sistema
 *      tags: [platos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Agregar'
 *      responses:
 *          201:
 *              description: plato creado
 *          400:
 *              description: peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.post('/', plato_no_repetido, agregar_plato);

/**
 * @swagger
 * /platos/{id_plato}:
 *  put:
 *      summary: Modifica un plato en el sistema
 *      tags: [platos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Agregar'
 *      parameters:
 *          - in: path
 *            name: id_plato
 *            schema:
 *                $ref: '#/components/schemas/Modificar'
 *            required: true
 *      responses:
 *          200:
 *              description: platos modificado
 *          400:
 *              description: Peticion no valida
 *          401:
 *              description: Usuario aun no ha iniciado sesion
 */
router.put('/:id_plato', plato_existe, modificar_plato)

/**
 * @swagger
 * tags:
 *  name: platos
 *  description: Seccion de metodos de platos 
 * 
 * components: 
 *  schemas:
 *      Agregar:
 *          type: object
 *          required:
 *              -nombre_plato
 *              -precio
 *          properties:
 *              nombre_platos:
 *                  type: string
 *                  description: Nombre del plato  
 *              precio:
 *                  type: integer
 *                  description: Precio del plato 
 *          example:  
 *              name:  name 30
 *              precio: 68
 *      Modficar:
 *          type: object
 *          required:
 *              -id_plato
 *          properties:
 *              id_plato:
 *                  type: integer
 *                  description: Id del _platos 
 *              
 */
module.exports=router;
