const autenticacion = (err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json('Token invalido');
    } else {
        res.status(500).json('Internal server error');
    }
    if(!err) next()
};

module.exports = autenticacion