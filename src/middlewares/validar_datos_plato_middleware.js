const platos_schema = require('../Schemas/platos.joi');

const datos_correctos_plato = async (req, res, next)=>{
    try{
        await platos_schema.validateAsync(req.body);
        next();
    } catch(e){
        console.error(e.details[0].message);
        res.status(404).json(e.details[0].message);
    }
}

module.exports= datos_correctos_plato;
