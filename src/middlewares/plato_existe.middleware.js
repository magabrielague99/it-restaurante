const fetch = require('node-fetch');

const plato_existe = async (req, res, next) => {
    const {id_plato} = req.params;
        const plato =await fetch(`https://619cead568ebaa001753cdbb.mockapi.io/api/platos/${id_plato}`)
            .then((respuesta) => {
                return respuesta.json();
            })
            .catch(err => {  
            res.status(404).json("Plato no encontrado"); 
            console.log(err); 
        })

        if(plato == "Not found"){
            res.status(400).json("plato no encontrado")
        }else next();
}

module.exports = plato_existe;
