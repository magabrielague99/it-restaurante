const Joi = require('joi');

const platos_schema = Joi.object({
    name: Joi.string()
        .min(3)
        .max(30)
        .required(),
        
    precio: Joi.number()
        .min(0)
        .required()
});

module.exports = platos_schema;
