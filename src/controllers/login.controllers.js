const jsonwebtoken = require('jsonwebtoken');
require('dotenv').config();
const { configuracion } = require('../utils/config');

const usuario = configuracion.Usuario;

const token = jsonwebtoken.sign({
    usuario,
}, configuracion.Contrasenia);

module.exports= {token};