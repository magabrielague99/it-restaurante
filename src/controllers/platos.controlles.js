const fetch = require('node-fetch');
require('dotenv').config();
const { configuracion } = require('../utils/config');

const url_mockApi = configuracion.MockAPi;

const obtener_plato = async (req, res) => {
    await fetch(`https://${url_mockApi}`)
        .then((respuesta) => {
            return respuesta.json();
        })
        .then((json) => {
            res.status(200).json(json);
            console.log(json);
        })
        .catch(err => { console.log(err); });
}

const eliminar_plato = async (req, res) => {
    const { id_plato } = req.params;
    await fetch(`https://${url_mockApi}/${id_plato}`, {
        method: 'DELETE',
        
    })
        .then((respuesta) => {

            return respuesta.json();
        })
        .then((json) => {
            res.status(200).json("Plato elimnado")
            console.log(json);
        })
        .catch(err => {
            res.status(400).json("Error en peticion");
            console.log(err);
        });
}

const agregar_plato = async (req, res) => {
    const plato = req.body;
    await fetch(`https://${url_mockApi}`, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8' 
        },
        body: JSON.stringify(plato)
    })
        .then((respuesta) => {
            return respuesta.json();
        })
        .then((json) => {
            res.json(json);
            console.log(json);
        })
        .catch(err => {
            console.log(err);
        });
}

const modificar_plato = async (req, res) => {
    const plato = req.body;
    console.log(plato);
    const {id_plato} = req.params;
    await fetch(`https://${url_mockApi}/${id_plato}`, {
        method: 'PUT',
        headers: {
            'Content-type': 'application/json; charset=UTF-8' 
        },
        body: JSON.stringify(plato) 
    })
        .then((respuesta) => {
            return respuesta.json();
        })
        .then((json) => {
            res.json(json);
            console.log(json);
        })
        .catch(err => {
            console.log(err);
        });
}

module.exports = { obtener_plato, eliminar_plato, agregar_plato, modificar_plato };