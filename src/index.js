const express = require('express');
const helmet = require("helmet");
const swagger= require('./utils/swagger');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const expressJwt = require('express-jwt');
require('dotenv').config();
const { configuracion } = require('./utils/config');
const autenticacion = require('./middlewares/autenticacion.middleware')
const {token} = require('./controllers/login.controllers');
const platos_route = require('./routes/platos.route');

const app = express();
const port = configuracion.Puerto;

console.log(`token: ${token}`);

app.use(helmet());

app.use(express.json());


const swaggerSpecs = swaggerJsDoc(swagger);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use(expressJwt({
    secret: configuracion.Contrasenia,
    algorithms: ['HS256'],
}));

app.use(autenticacion);

app.use('/platos', platos_route);

app.listen(port, () => { console.log(`Corriendo en el puerto ${port}`); })