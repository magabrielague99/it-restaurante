configuracion = {
    Contrasenia: process.env.JWT_contrasenia_segura,
    Puerto: process.env.puerto,
    Usuario: process.env.usuario,
    MockAPi: process.env.mockApi
};

module.exports = { configuracion }
