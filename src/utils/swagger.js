const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Restaurante",
            version: "1.0.0",
            description: "Maria Gabriela Guerrero"
        },
        servers: [
            {
                url: "http://localhost:3000",
                description: "Online server"
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth:{
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            }
        },
        security: [
            {
                bearerAuth: []
            }
        ]
    },
    apis: ["src/routes/*.js"]
};

module.exports = swaggerOptions;
