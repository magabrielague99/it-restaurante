# RESTAURANTE IT.
## Recursos
- Node.js
- Express.js
- JWT
- Swagger
- Joi
- Heltmet

## MockAPi
API con platos ficticios.

acceso [MockApi](https://mockapi.io/projects/619cead568ebaa001753cdbc) para pruebas con la API

estructura: {id, name, precio}

## Ejecucion
#### 1. Clonar el proyecto 
```
git clone https://gitlab.com/magabrielague99/it-restaurante.git
```
#### 2. Correr proyecto
```
npm run start 
```
#### 3. Acceso
```` 
Se puede acceder en http://localhost:3000/api-docs
```` 

## A tener en cuenta
````
al ejecutar el proyecto se crea por defecto un usuario al que se le asigna un JWT, con el cual puede acceder a los endpoints de plato
````
